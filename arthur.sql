-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : Dim 28 fév. 2021 à 11:20
-- Version du serveur :  5.7.24
-- Version de PHP : 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `arthur`
--

-- --------------------------------------------------------

--
-- Structure de la table `wp_booking`
--

CREATE TABLE `wp_booking` (
  `booking_id` bigint(20) UNSIGNED NOT NULL,
  `trash` bigint(10) NOT NULL DEFAULT '0',
  `sync_gid` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_new` bigint(10) NOT NULL DEFAULT '1',
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `sort_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `form` text COLLATE utf8mb4_unicode_520_ci,
  `booking_type` bigint(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `wp_booking`
--

INSERT INTO `wp_booking` (`booking_id`, `trash`, `sync_gid`, `is_new`, `status`, `sort_date`, `modification_date`, `form`, `booking_type`) VALUES
(1, 0, '', 1, '', '2020-12-25 00:00:00', '2020-12-23 10:21:12', 'text^name1^Jony~text^secondname1^Smith~text^email1^example-free@wpbookingcalendar.com~text^phone1^458-77-77~textarea^details1^Reserve a room with sea view', 1),
(3, 0, '', 1, '', '2021-03-04 00:00:00', '2020-12-23 10:41:30', 'text^name1^Paul~text^secondname1^Collas~email^email1^paulcollas446@gmail.com~text^phone1^0782506788~textarea^details1^Oklm', 1);

-- --------------------------------------------------------

--
-- Structure de la table `wp_bookingdates`
--

CREATE TABLE `wp_bookingdates` (
  `booking_dates_id` bigint(20) UNSIGNED NOT NULL,
  `booking_id` bigint(20) UNSIGNED NOT NULL,
  `booking_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `approved` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `wp_bookingdates`
--

INSERT INTO `wp_bookingdates` (`booking_dates_id`, `booking_id`, `booking_date`, `approved`) VALUES
(1, 1, '2020-12-25 00:00:00', 0),
(2, 1, '2020-12-26 00:00:00', 0),
(3, 1, '2020-12-27 00:00:00', 0),
(10, 3, '2021-03-04 00:00:00', 0),
(11, 3, '2021-03-11 00:00:00', 0),
(12, 3, '2021-03-18 00:00:00', 0),
(13, 3, '2021-03-24 00:00:00', 0),
(14, 3, '2021-03-25 00:00:00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Un commentateur WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-09-29 19:42:09', '2020-09-29 17:42:09', 'Bonjour, ceci est un commentaire.\nPour débuter avec la modération, la modification et la suppression de commentaires, veuillez visiter l’écran des Commentaires dans le Tableau de bord.\nLes avatars des personnes qui commentent arrivent depuis <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/07_MUCH/arthur', 'yes'),
(2, 'home', 'http://localhost/07_MUCH/arthur', 'yes'),
(3, 'blogname', 'Arthur Besnehard | Portfolio', 'yes'),
(4, 'blogdescription', 'Un site utilisant WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'paulcollas446@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F Y', 'yes'),
(24, 'time_format', 'G \\h i \\m\\i\\n', 'yes'),
(25, 'links_updated_date_format', 'j F Y G \\h i \\m\\i\\n', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:144:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:15:\"realisations/?$\";s:32:\"index.php?post_type=realisations\";s:45:\"realisations/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=realisations&feed=$matches[1]\";s:40:\"realisations/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=realisations&feed=$matches[1]\";s:32:\"realisations/page/([0-9]{1,})/?$\";s:50:\"index.php?post_type=realisations&paged=$matches[1]\";s:13:\"competence/?$\";s:30:\"index.php?post_type=competence\";s:43:\"competence/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?post_type=competence&feed=$matches[1]\";s:38:\"competence/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?post_type=competence&feed=$matches[1]\";s:30:\"competence/page/([0-9]{1,})/?$\";s:48:\"index.php?post_type=competence&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:40:\"realisations/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:50:\"realisations/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:70:\"realisations/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"realisations/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"realisations/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:46:\"realisations/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:29:\"realisations/([^/]+)/embed/?$\";s:45:\"index.php?realisations=$matches[1]&embed=true\";s:33:\"realisations/([^/]+)/trackback/?$\";s:39:\"index.php?realisations=$matches[1]&tb=1\";s:53:\"realisations/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?realisations=$matches[1]&feed=$matches[2]\";s:48:\"realisations/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?realisations=$matches[1]&feed=$matches[2]\";s:41:\"realisations/([^/]+)/page/?([0-9]{1,})/?$\";s:52:\"index.php?realisations=$matches[1]&paged=$matches[2]\";s:48:\"realisations/([^/]+)/comment-page-([0-9]{1,})/?$\";s:52:\"index.php?realisations=$matches[1]&cpage=$matches[2]\";s:37:\"realisations/([^/]+)(?:/([0-9]+))?/?$\";s:51:\"index.php?realisations=$matches[1]&page=$matches[2]\";s:29:\"realisations/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:39:\"realisations/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:59:\"realisations/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"realisations/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"realisations/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:35:\"realisations/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:46:\"genre/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?genre=$matches[1]&feed=$matches[2]\";s:41:\"genre/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?genre=$matches[1]&feed=$matches[2]\";s:22:\"genre/([^/]+)/embed/?$\";s:38:\"index.php?genre=$matches[1]&embed=true\";s:34:\"genre/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?genre=$matches[1]&paged=$matches[2]\";s:16:\"genre/([^/]+)/?$\";s:27:\"index.php?genre=$matches[1]\";s:38:\"competence/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:48:\"competence/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:68:\"competence/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"competence/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"competence/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:44:\"competence/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:27:\"competence/([^/]+)/embed/?$\";s:43:\"index.php?competence=$matches[1]&embed=true\";s:31:\"competence/([^/]+)/trackback/?$\";s:37:\"index.php?competence=$matches[1]&tb=1\";s:51:\"competence/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?competence=$matches[1]&feed=$matches[2]\";s:46:\"competence/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?competence=$matches[1]&feed=$matches[2]\";s:39:\"competence/([^/]+)/page/?([0-9]{1,})/?$\";s:50:\"index.php?competence=$matches[1]&paged=$matches[2]\";s:46:\"competence/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?competence=$matches[1]&cpage=$matches[2]\";s:35:\"competence/([^/]+)(?:/([0-9]+))?/?$\";s:49:\"index.php?competence=$matches[1]&page=$matches[2]\";s:27:\"competence/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"competence/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"competence/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"competence/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"competence/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"competence/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:40:\"([0-9]{4})/([0-9]{1,2})/([^/]+)/embed/?$\";s:75:\"index.php?year=$matches[1]&monthnum=$matches[2]&name=$matches[3]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:69:\"index.php?year=$matches[1]&monthnum=$matches[2]&name=$matches[3]&tb=1\";s:64:\"([0-9]{4})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&name=$matches[3]&feed=$matches[4]\";s:59:\"([0-9]{4})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&name=$matches[3]&feed=$matches[4]\";s:52:\"([0-9]{4})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&name=$matches[3]&paged=$matches[4]\";s:59:\"([0-9]{4})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&name=$matches[3]&cpage=$matches[4]\";s:48:\"([0-9]{4})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&name=$matches[3]&page=$matches[4]\";s:36:\"[0-9]{4}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"[0-9]{4}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"[0-9]{4}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"[0-9]{4}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"[0-9]{4}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"[0-9]{4}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:30:\"advanced-custom-fields/acf.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'arthur-portfolio', 'yes'),
(41, 'stylesheet', 'arthur-portfolio', 'yes'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '48748', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'Europe/Paris', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1616953328', 'yes'),
(94, 'initial_db_version', '47018', 'yes'),
(95, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'WPLANG', 'fr_FR', 'yes'),
(98, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:6:{i:1614512531;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1614534130;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1614534131;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1614534151;a:3:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1614793330;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'recovery_keys', 'a:0:{}', 'yes'),
(121, 'theme_mods_twentytwenty', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1605562556;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(127, 'current_theme', '', 'yes'),
(128, 'theme_mods_arthur-portfolio', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1608663382;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(129, 'theme_switched', '', 'yes'),
(130, 'recovery_mode_email_last_sent', '1601402777', 'yes'),
(135, '_transient_health-check-site-status-result', '{\"good\":10,\"recommended\":10,\"critical\":0}', 'yes'),
(246, 'recently_activated', 'a:1:{s:25:\"booking/wpdev-booking.php\";i:1614510289;}', 'yes'),
(255, 'acf_version', '5.9.1', 'yes'),
(312, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:23:\"paulcollas446@gmail.com\";s:7:\"version\";s:5:\"5.4.4\";s:9:\"timestamp\";i:1604305732;}', 'no'),
(368, 'category_children', 'a:0:{}', 'yes'),
(442, 'disallowed_keys', '', 'no'),
(443, 'comment_previously_approved', '1', 'yes'),
(444, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(445, 'finished_updating_comment_type', '1', 'yes'),
(446, 'db_upgraded', '', 'yes'),
(449, 'can_compress_scripts', '1', 'no'),
(476, 'theme_mods_travern', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1614510250;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-1\";a:0:{}}}}', 'yes'),
(490, 'booking_activation_process', 'Off', 'yes'),
(491, 'booking_admin_cal_count', '2', 'yes'),
(492, 'booking_skin', '/css/skins/traditional.css', 'yes'),
(493, 'booking_num_per_page', '10', 'yes'),
(494, 'booking_sort_order', '', 'yes'),
(495, 'booking_default_toolbar_tab', 'filter', 'yes'),
(496, 'booking_listing_default_view_mode', 'vm_calendar', 'yes'),
(497, 'booking_view_days_num', '90', 'yes'),
(498, 'booking_max_monthes_in_calendar', '1y', 'yes'),
(499, 'booking_client_cal_count', '1', 'yes'),
(500, 'booking_start_day_weeek', '0', 'yes'),
(501, 'booking_title_after_reservation', 'Merci pour votre réservation.  Nous vous enverrons une confirmation de votre réservation le plus tôt possible.', 'yes'),
(502, 'booking_title_after_reservation_time', '7000', 'yes'),
(503, 'booking_type_of_thank_you_message', 'message', 'yes'),
(504, 'booking_thank_you_page_URL', '/thank-you', 'yes'),
(505, 'booking_is_use_autofill_4_logged_user', 'Off', 'yes'),
(506, 'booking_date_format', 'j F Y', 'yes'),
(507, 'booking_is_use_localized_time_format', 'Off', 'yes'),
(508, 'booking_date_view_type', 'short', 'yes'),
(509, 'booking_is_delete_if_deactive', 'Off', 'yes'),
(510, 'booking_dif_colors_approval_pending', 'On', 'yes'),
(511, 'booking_is_use_hints_at_admin_panel', 'On', 'yes'),
(512, 'booking_is_not_load_bs_script_in_client', 'Off', 'yes'),
(513, 'booking_is_not_load_bs_script_in_admin', 'Off', 'yes'),
(514, 'booking_is_load_js_css_on_specific_pages', 'Off', 'yes'),
(515, 'booking_is_show_system_debug_log', 'Off', 'yes'),
(516, 'booking_pages_for_load_js_css', '', 'yes'),
(517, 'booking_type_of_day_selections', 'multiple', 'yes'),
(518, 'booking_timeslot_picker', 'Off', 'yes'),
(519, 'booking_timeslot_picker_skin', '/css/time_picker_skins/grey.css', 'yes'),
(520, 'booking_timeslot_day_bg_as_available', 'On', 'yes'),
(521, 'booking_form_is_using_bs_css', 'On', 'yes'),
(522, 'booking_form_format_type', 'vertical', 'yes'),
(523, 'booking_form_field_active1', 'On', 'yes'),
(524, 'booking_form_field_required1', 'On', 'yes'),
(525, 'booking_form_field_label1', 'First Name', 'yes'),
(526, 'booking_form_field_active2', 'On', 'yes'),
(527, 'booking_form_field_required2', 'On', 'yes'),
(528, 'booking_form_field_label2', 'Last Name', 'yes'),
(529, 'booking_form_field_active3', 'On', 'yes'),
(530, 'booking_form_field_required3', 'On', 'yes'),
(531, 'booking_form_field_label3', 'Email', 'yes'),
(532, 'booking_form_field_active4', 'On', 'yes'),
(533, 'booking_form_field_required4', 'Off', 'yes'),
(534, 'booking_form_field_label4', 'Phone', 'yes'),
(535, 'booking_form_field_active5', 'On', 'yes'),
(536, 'booking_form_field_required5', 'Off', 'yes'),
(537, 'booking_form_field_label5', 'Details', 'yes'),
(538, 'booking_form_field_active6', 'Off', 'yes'),
(539, 'booking_form_field_required6', 'Off', 'yes'),
(540, 'booking_form_field_label6', 'Visitors', 'yes'),
(541, 'booking_form_field_values6', '1\n2\n3\n4', 'yes'),
(542, 'booking_is_days_always_available', 'Off', 'yes'),
(543, 'booking_is_show_pending_days_as_available', 'Off', 'yes'),
(544, 'booking_check_on_server_if_dates_free', 'Off', 'yes'),
(545, 'booking_unavailable_days_num_from_today', '0', 'yes'),
(546, 'booking_unavailable_day0', 'Off', 'yes'),
(547, 'booking_unavailable_day1', 'Off', 'yes'),
(548, 'booking_unavailable_day2', 'Off', 'yes'),
(549, 'booking_unavailable_day3', 'Off', 'yes'),
(550, 'booking_unavailable_day4', 'Off', 'yes'),
(551, 'booking_unavailable_day5', 'Off', 'yes'),
(552, 'booking_unavailable_day6', 'Off', 'yes'),
(553, 'booking_menu_position', 'top', 'yes'),
(554, 'booking_user_role_booking', 'editor', 'yes'),
(555, 'booking_user_role_addbooking', 'editor', 'yes'),
(556, 'booking_user_role_resources', 'editor', 'yes'),
(557, 'booking_user_role_settings', 'administrator', 'yes'),
(558, 'booking_is_email_reservation_adress', 'On', 'yes'),
(559, 'booking_email_reservation_adress', '&quot;Booking system&quot; &lt;paulcollas446@gmail.com&gt;', 'yes'),
(560, 'booking_email_reservation_from_adress', '[visitoremail]', 'yes'),
(561, 'booking_email_reservation_subject', 'Nouvelle réservation', 'yes'),
(562, 'booking_email_reservation_content', 'Vous devez d\'approuver une nouvelle réservation [bookingtype] pour [dates]&lt;br/&gt;&lt;br/&gt;Informations détaillées:&lt;br/&gt; [content]&lt;br/&gt;&lt;br/&gt; Actuellement une nouvelle réservation est en attente d\'approbation. S\'il vous plaît visitez le panneau de modération [moderatelink]&lt;br/&gt;&lt;br/&gt;  Merci, Arthur Besnehard | Portfolio&lt;br/&gt;[siteurl]', 'yes'),
(563, 'booking_is_email_newbookingbyperson_adress', 'Off', 'yes'),
(564, 'booking_email_newbookingbyperson_adress', '&quot;Booking system&quot; &lt;paulcollas446@gmail.com&gt;', 'yes'),
(565, 'booking_email_newbookingbyperson_subject', 'Nouvelle réservation', 'yes'),
(566, 'booking_email_newbookingbyperson_content', 'Votre réservation [bookingtype] pour:[dates] est en cours de traitement! Nous vous enverrons une confirmation par mail. &lt;br/&gt;&lt;br/&gt;[content]&lt;br/&gt;&lt;br/&gt; Merci,Arthur Besnehard | Portfolio&lt;br/&gt;[siteurl]', 'yes'),
(567, 'booking_is_email_approval_adress', 'On', 'yes'),
(568, 'booking_is_email_approval_send_copy_to_admin', 'Off', 'yes'),
(569, 'booking_email_approval_adress', '&quot;Booking system&quot; &lt;paulcollas446@gmail.com&gt;', 'yes'),
(570, 'booking_email_approval_subject', 'Votre réservation a été approuvée', 'yes'),
(571, 'booking_email_approval_content', 'Votre réservation [bookingtype] pour : [dates]a été approuvée. &lt;br/&gt;&lt;br/&gt;[content]&lt;br/&gt;&lt;br/&gt;Merci, Arthur Besnehard | Portfolio&lt;br/&gt;[siteurl]', 'yes'),
(572, 'booking_is_email_deny_adress', 'On', 'yes'),
(573, 'booking_is_email_deny_send_copy_to_admin', 'Off', 'yes'),
(574, 'booking_email_deny_adress', '&quot;Booking system&quot; &lt;paulcollas446@gmail.com&gt;', 'yes'),
(575, 'booking_email_deny_subject', 'Votre réservation a été refusée', 'yes'),
(576, 'booking_email_deny_content', 'Votre réservation [bookingtype] pour [dates] a été annulée. &lt;br/&gt;[denyreason]&lt;br/&gt;&lt;br/&gt;[content]&lt;br/&gt;&lt;br/&gt;Merci, Arthur Besnehard | Portfolio&lt;br/&gt;[siteurl]', 'yes'),
(577, 'booking_widget_title', 'Formulaire de réservation', 'yes'),
(578, 'booking_widget_show', 'booking_form', 'yes'),
(579, 'booking_widget_type', '1', 'yes'),
(580, 'booking_widget_calendar_count', '1', 'yes'),
(581, 'booking_widget_last_field', '', 'yes'),
(582, 'booking_wpdev_copyright_adminpanel', 'On', 'yes'),
(583, 'booking_is_show_powered_by_notice', 'Off', 'yes'),
(584, 'booking_is_use_captcha', 'On', 'yes'),
(585, 'booking_is_show_legend', 'Off', 'yes'),
(586, 'booking_legend_is_show_item_available', 'On', 'yes'),
(587, 'booking_legend_text_for_item_available', 'Disponible', 'yes'),
(588, 'booking_legend_is_show_item_pending', 'On', 'yes'),
(589, 'booking_legend_text_for_item_pending', 'En attente', 'yes'),
(590, 'booking_legend_is_show_item_approved', 'On', 'yes'),
(591, 'booking_legend_text_for_item_approved', 'Réservé', 'yes'),
(592, 'booking_legend_is_show_numbers', 'Off', 'yes'),
(593, 'booking_email_new_admin', 'a:15:{s:7:\"enabled\";s:2:\"On\";s:2:\"to\";s:23:\"paulcollas446@gmail.com\";s:7:\"to_name\";s:14:\"Booking system\";s:4:\"from\";s:23:\"paulcollas446@gmail.com\";s:9:\"from_name\";s:14:\"Booking system\";s:7:\"subject\";s:21:\"Nouvelle réservation\";s:7:\"content\";s:332:\"Vous devez d\'approuver une nouvelle réservation [bookingtype] pour [dates]<br/><br/>Informations détaillées:<br/> [content]<br/><br/> Actuellement une nouvelle réservation est en attente d\'approbation. S\'il vous plaît visitez le panneau de modération [moderatelink]<br/><br/>  Merci, Arthur Besnehard | Portfolio<br/>[siteurl]\";s:14:\"header_content\";s:0:\"\";s:14:\"footer_content\";s:0:\"\";s:13:\"template_file\";s:5:\"plain\";s:10:\"base_color\";s:7:\"#557da1\";s:16:\"background_color\";s:7:\"#f5f5f5\";s:10:\"body_color\";s:7:\"#fdfdfd\";s:10:\"text_color\";s:7:\"#505050\";s:18:\"email_content_type\";s:4:\"html\";}', 'yes'),
(594, 'booking_email_new_visitor', 'a:13:{s:7:\"enabled\";s:2:\"On\";s:4:\"from\";s:23:\"paulcollas446@gmail.com\";s:9:\"from_name\";s:14:\"Booking system\";s:7:\"subject\";s:21:\"Nouvelle réservation\";s:7:\"content\";s:199:\"Votre réservation [bookingtype] pour:[dates] est en cours de traitement! Nous vous enverrons une confirmation par mail. <br/><br/>[content]<br/><br/> Merci,Arthur Besnehard | Portfolio<br/>[siteurl]\";s:14:\"header_content\";s:0:\"\";s:14:\"footer_content\";s:0:\"\";s:13:\"template_file\";s:5:\"plain\";s:10:\"base_color\";s:7:\"#557da1\";s:16:\"background_color\";s:7:\"#f5f5f5\";s:10:\"body_color\";s:7:\"#fdfdfd\";s:10:\"text_color\";s:7:\"#505050\";s:18:\"email_content_type\";s:4:\"html\";}', 'yes'),
(595, 'booking_email_approved', 'a:16:{s:7:\"enabled\";s:2:\"On\";s:13:\"copy_to_admin\";s:3:\"Off\";s:2:\"to\";s:23:\"paulcollas446@gmail.com\";s:7:\"to_name\";s:14:\"Booking system\";s:4:\"from\";s:23:\"paulcollas446@gmail.com\";s:9:\"from_name\";s:14:\"Booking system\";s:7:\"subject\";s:37:\"Votre réservation a été approuvée\";s:7:\"content\";s:145:\"Votre réservation [bookingtype] pour : [dates]a été approuvée. <br/><br/>[content]<br/><br/>Merci, Arthur Besnehard | Portfolio<br/>[siteurl]\";s:14:\"header_content\";s:0:\"\";s:14:\"footer_content\";s:0:\"\";s:13:\"template_file\";s:5:\"plain\";s:10:\"base_color\";s:7:\"#557da1\";s:16:\"background_color\";s:7:\"#f5f5f5\";s:10:\"body_color\";s:7:\"#fdfdfd\";s:10:\"text_color\";s:7:\"#505050\";s:18:\"email_content_type\";s:4:\"html\";}', 'yes'),
(596, 'booking_email_deleted', 'a:16:{s:7:\"enabled\";s:2:\"On\";s:13:\"copy_to_admin\";s:3:\"Off\";s:2:\"to\";s:23:\"paulcollas446@gmail.com\";s:7:\"to_name\";s:14:\"Booking system\";s:4:\"from\";s:23:\"paulcollas446@gmail.com\";s:9:\"from_name\";s:14:\"Booking system\";s:7:\"subject\";s:35:\"Votre réservation a été refusée\";s:7:\"content\";s:159:\"Votre réservation [bookingtype] pour [dates] a été annulée. <br/>[denyreason]<br/><br/>[content]<br/><br/>Merci, Arthur Besnehard | Portfolio<br/>[siteurl]\";s:14:\"header_content\";s:0:\"\";s:14:\"footer_content\";s:0:\"\";s:13:\"template_file\";s:5:\"plain\";s:10:\"base_color\";s:7:\"#557da1\";s:16:\"background_color\";s:7:\"#f5f5f5\";s:10:\"body_color\";s:7:\"#fdfdfd\";s:10:\"text_color\";s:7:\"#505050\";s:18:\"email_content_type\";s:4:\"html\";}', 'yes'),
(597, 'booking_email_deny', 'a:16:{s:7:\"enabled\";s:2:\"On\";s:13:\"copy_to_admin\";s:3:\"Off\";s:2:\"to\";s:23:\"paulcollas446@gmail.com\";s:7:\"to_name\";s:14:\"Booking system\";s:4:\"from\";s:23:\"paulcollas446@gmail.com\";s:9:\"from_name\";s:14:\"Booking system\";s:7:\"subject\";s:35:\"Votre réservation a été refusée\";s:7:\"content\";s:159:\"Votre réservation [bookingtype] pour [dates] a été annulée. <br/>[denyreason]<br/><br/>[content]<br/><br/>Merci, Arthur Besnehard | Portfolio<br/>[siteurl]\";s:14:\"header_content\";s:0:\"\";s:14:\"footer_content\";s:0:\"\";s:13:\"template_file\";s:5:\"plain\";s:10:\"base_color\";s:7:\"#557da1\";s:16:\"background_color\";s:7:\"#f5f5f5\";s:10:\"body_color\";s:7:\"#fdfdfd\";s:10:\"text_color\";s:7:\"#505050\";s:18:\"email_content_type\";s:4:\"html\";}', 'yes'),
(598, 'booking_email_trash', 'a:16:{s:7:\"enabled\";s:2:\"On\";s:13:\"copy_to_admin\";s:3:\"Off\";s:2:\"to\";s:23:\"paulcollas446@gmail.com\";s:7:\"to_name\";s:14:\"Booking system\";s:4:\"from\";s:23:\"paulcollas446@gmail.com\";s:9:\"from_name\";s:14:\"Booking system\";s:7:\"subject\";s:35:\"Votre réservation a été refusée\";s:7:\"content\";s:159:\"Votre réservation [bookingtype] pour [dates] a été annulée. <br/>[denyreason]<br/><br/>[content]<br/><br/>Merci, Arthur Besnehard | Portfolio<br/>[siteurl]\";s:14:\"header_content\";s:0:\"\";s:14:\"footer_content\";s:0:\"\";s:13:\"template_file\";s:5:\"plain\";s:10:\"base_color\";s:7:\"#557da1\";s:16:\"background_color\";s:7:\"#f5f5f5\";s:10:\"body_color\";s:7:\"#fdfdfd\";s:10:\"text_color\";s:7:\"#505050\";s:18:\"email_content_type\";s:4:\"html\";}', 'yes'),
(599, 'booking_form_structure_type', 'vertical', 'yes'),
(600, 'booking_menu_go_pro', 'show', 'yes'),
(601, 'booking_ics_force_import', 'Off', 'yes'),
(602, 'booking_ics_force_trash_before_import', 'Off', 'yes'),
(603, 'booking_form', '<div class=\"wpbc_booking_form_structure wpbc_vertical\">\n  <div class=\"wpbc_structure_calendar\">\n    [calendar]\n  </div>\n  <div class=\"wpbc_structure_form\">\n     <p>First Name*:<br />[text* name]</p>\n     <p>Last Name*:<br />[text* secondname]</p>\n     <p>Email*:<br />[email* email]</p>\n     <p>Phone:<br />[text phone]</p>\n     <p>Details:<br />[textarea details]</p>\n     <p>[captcha]</p>\n     <p>[submit class:btn \"Send\"]</p>\n  </div>\n</div>\n<div class=\"wpbc_booking_form_footer\"></div>', 'yes'),
(604, 'booking_form_show', '<div style=\"text-align:left;word-wrap: break-word;\">\n  <strong>First Name</strong>: <span class=\"fieldvalue\">[name]</span><br/>\n  <strong>Last Name</strong>: <span class=\"fieldvalue\">[secondname]</span><br/>\n  <strong>Email</strong>: <span class=\"fieldvalue\">[email]</span><br/>\n  <strong>Phone</strong>: <span class=\"fieldvalue\">[phone]</span><br/>\n  <strong>Details</strong>: <span class=\"fieldvalue\">[details]</span><br/>\n</div>', 'yes'),
(605, 'booking_form_visual', 'a:9:{i:0;a:2:{s:4:\"type\";s:8:\"calendar\";s:10:\"obligatory\";s:2:\"On\";}i:1;a:6:{s:4:\"type\";s:4:\"text\";s:4:\"name\";s:4:\"name\";s:10:\"obligatory\";s:3:\"Off\";s:6:\"active\";s:2:\"On\";s:8:\"required\";s:2:\"On\";s:5:\"label\";s:10:\"First Name\";}i:2;a:6:{s:4:\"type\";s:4:\"text\";s:4:\"name\";s:10:\"secondname\";s:10:\"obligatory\";s:3:\"Off\";s:6:\"active\";s:2:\"On\";s:8:\"required\";s:2:\"On\";s:5:\"label\";s:9:\"Last Name\";}i:3;a:6:{s:4:\"type\";s:5:\"email\";s:4:\"name\";s:5:\"email\";s:10:\"obligatory\";s:2:\"On\";s:6:\"active\";s:2:\"On\";s:8:\"required\";s:2:\"On\";s:5:\"label\";s:5:\"Email\";}i:4;a:7:{s:4:\"type\";s:6:\"select\";s:4:\"name\";s:8:\"visitors\";s:10:\"obligatory\";s:3:\"Off\";s:6:\"active\";s:3:\"Off\";s:8:\"required\";s:3:\"Off\";s:5:\"label\";s:8:\"Visitors\";s:5:\"value\";s:7:\"1\n2\n3\n4\";}i:5;a:6:{s:4:\"type\";s:4:\"text\";s:4:\"name\";s:5:\"phone\";s:10:\"obligatory\";s:3:\"Off\";s:6:\"active\";s:2:\"On\";s:8:\"required\";s:3:\"Off\";s:5:\"label\";s:5:\"Phone\";}i:6;a:6:{s:4:\"type\";s:8:\"textarea\";s:4:\"name\";s:7:\"details\";s:10:\"obligatory\";s:3:\"Off\";s:6:\"active\";s:2:\"On\";s:8:\"required\";s:3:\"Off\";s:5:\"label\";s:7:\"Details\";}i:7;a:6:{s:4:\"type\";s:7:\"captcha\";s:4:\"name\";s:7:\"captcha\";s:10:\"obligatory\";s:2:\"On\";s:6:\"active\";s:3:\"Off\";s:8:\"required\";s:2:\"On\";s:5:\"label\";s:0:\"\";}i:8;a:6:{s:4:\"type\";s:6:\"submit\";s:4:\"name\";s:6:\"submit\";s:10:\"obligatory\";s:2:\"On\";s:6:\"active\";s:2:\"On\";s:8:\"required\";s:2:\"On\";s:5:\"label\";s:7:\"Envoyer\";}}', 'yes'),
(606, 'booking_gcal_feed', '', 'yes'),
(607, 'booking_gcal_events_from', 'month-start', 'yes'),
(608, 'booking_gcal_events_from_offset', '', 'yes'),
(609, 'booking_gcal_events_from_offset_type', '', 'yes'),
(610, 'booking_gcal_events_until', 'any', 'yes'),
(611, 'booking_gcal_events_until_offset', '', 'yes'),
(612, 'booking_gcal_events_until_offset_type', '', 'yes'),
(613, 'booking_gcal_events_max', '25', 'yes'),
(614, 'booking_gcal_api_key', '', 'yes'),
(615, 'booking_gcal_timezone', '', 'yes'),
(616, 'booking_gcal_is_send_email', 'Off', 'yes'),
(617, 'booking_gcal_auto_import_is_active', 'Off', 'yes'),
(618, 'booking_gcal_auto_import_time', '24', 'yes'),
(619, 'booking_gcal_events_form_fields', 's:101:\"a:3:{s:5:\"title\";s:9:\"text^name\";s:11:\"description\";s:16:\"textarea^details\";s:5:\"where\";s:5:\"text^\";}\";', 'yes'),
(620, 'booking_version_num', '8.8', 'yes'),
(623, 'widget_bookingwidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(624, 'booking_activation_redirect_for_version', '8.8', 'yes'),
(630, '_transient_is_multi_author', '0', 'yes'),
(632, '_site_transient_timeout_theme_roots', '1614512028', 'no'),
(633, '_site_transient_theme_roots', 'a:5:{s:16:\"arthur-portfolio\";s:7:\"/themes\";s:7:\"travern\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";}', 'no'),
(634, '_site_transient_timeout_php_check_9dfe9c1407d8720f2aa82fbeb25ecdd3', '1615115029', 'no'),
(635, '_site_transient_php_check_9dfe9c1407d8720f2aa82fbeb25ecdd3', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(639, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:5:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/fr_FR/wordpress-5.6.2.zip\";s:6:\"locale\";s:5:\"fr_FR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/fr_FR/wordpress-5.6.2.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.6.2\";s:7:\"version\";s:5:\"5.6.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.6.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.6.2-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.6.2\";s:7:\"version\";s:5:\"5.6.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/fr_FR/wordpress-5.6.2.zip\";s:6:\"locale\";s:5:\"fr_FR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/fr_FR/wordpress-5.6.2.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.6.2\";s:7:\"version\";s:5:\"5.6.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/fr_FR/wordpress-5.6.1.zip\";s:6:\"locale\";s:5:\"fr_FR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/fr_FR/wordpress-5.6.1.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.6.1\";s:7:\"version\";s:5:\"5.6.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:4;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:63:\"https://downloads.wordpress.org/release/fr_FR/wordpress-5.6.zip\";s:6:\"locale\";s:5:\"fr_FR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:63:\"https://downloads.wordpress.org/release/fr_FR/wordpress-5.6.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:3:\"5.6\";s:7:\"version\";s:3:\"5.6\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1614510244;s:15:\"version_checked\";s:5:\"5.5.3\";s:12:\"translations\";a:0:{}}', 'no'),
(640, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1614510246;s:7:\"checked\";a:5:{s:16:\"arthur-portfolio\";s:0:\"\";s:7:\"travern\";s:3:\"1.1\";s:14:\"twentynineteen\";s:3:\"1.5\";s:15:\"twentyseventeen\";s:3:\"2.3\";s:12:\"twentytwenty\";s:3:\"1.2\";}s:8:\"response\";a:3:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.9\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.9.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentyseventeen\";a:6:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.5\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.5.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.1.6.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:9:\"no_update\";a:1:{s:7:\"travern\";a:6:{s:5:\"theme\";s:7:\"travern\";s:11:\"new_version\";s:3:\"1.1\";s:3:\"url\";s:37:\"https://wordpress.org/themes/travern/\";s:7:\"package\";s:53:\"https://downloads.wordpress.org/theme/travern.1.1.zip\";s:8:\"requires\";b:0;s:12:\"requires_php\";b:0;}}s:12:\"translations\";a:0:{}}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(641, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1614510246;s:7:\"checked\";a:3:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.9.1\";s:25:\"booking/wpdev-booking.php\";s:3:\"8.8\";s:51:\"restaurant-reservations/restaurant-reservations.php\";s:5:\"2.2.2\";}s:8:\"response\";a:1:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.9.5\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.9.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.6.2\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:2:{s:25:\"booking/wpdev-booking.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/booking\";s:4:\"slug\";s:7:\"booking\";s:6:\"plugin\";s:25:\"booking/wpdev-booking.php\";s:11:\"new_version\";s:3:\"8.8\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/booking/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/booking.8.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:60:\"https://ps.w.org/booking/assets/icon-256x256.png?rev=1730848\";s:2:\"1x\";s:60:\"https://ps.w.org/booking/assets/icon-128x128.png?rev=1730848\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:62:\"https://ps.w.org/booking/assets/banner-772x250.png?rev=1623635\";}s:11:\"banners_rtl\";a:0:{}}s:51:\"restaurant-reservations/restaurant-reservations.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:37:\"w.org/plugins/restaurant-reservations\";s:4:\"slug\";s:23:\"restaurant-reservations\";s:6:\"plugin\";s:51:\"restaurant-reservations/restaurant-reservations.php\";s:11:\"new_version\";s:5:\"2.2.2\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/restaurant-reservations/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/restaurant-reservations.2.2.2.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:76:\"https://ps.w.org/restaurant-reservations/assets/icon-128x128.png?rev=2205491\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:78:\"https://ps.w.org/restaurant-reservations/assets/banner-772x250.png?rev=2205491\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Structure de la table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 6, '_edit_lock', '1605984724:1'),
(10, 1, '_edit_lock', '1603804997:1'),
(11, 10, '_edit_last', '1'),
(12, 10, '_edit_lock', '1603882173:1'),
(13, 17, '_edit_last', '1'),
(14, 17, '_edit_lock', '1603881969:1'),
(15, 23, '_wp_attached_file', '2020/10/0.jpg'),
(16, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:13:\"2020/10/0.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"0-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"0-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:13:\"0-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"products\";a:4:{s:4:\"file\";s:13:\"0-400x400.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(17, 10, 'youtube', ''),
(18, 10, '_youtube', 'field_5f994c72222b3'),
(19, 24, 'youtube', ''),
(20, 24, '_youtube', 'field_5f994c72222b3'),
(21, 2, '_edit_lock', '1603888879:1'),
(24, 28, '_edit_lock', '1608717056:1'),
(25, 28, '_wp_page_template', 'about.php'),
(26, 31, '_edit_lock', '1604939728:1'),
(27, 31, '_wp_page_template', 'contact.php'),
(33, 36, '_edit_last', '1'),
(34, 36, '_edit_lock', '1604768388:1'),
(35, 6, '_edit_last', '1'),
(36, 46, '_edit_last', '1'),
(37, 46, '_edit_lock', '1604331990:1'),
(38, 53, '_edit_last', '1'),
(39, 53, '_edit_lock', '1605566270:1'),
(40, 6, 'subtitle-1', 'Bonjour, je suis '),
(41, 6, '_subtitle-1', 'field_5fa02b3b3c83d'),
(42, 6, 'title-1', 'Arthur Besnehard'),
(43, 6, '_title-1', 'field_5fa02b4a3c83e'),
(44, 6, 'subtitle-2', 'Hello, I\'m'),
(45, 6, '_subtitle-2', 'field_5fa02b573c83f'),
(46, 6, 'title-2', 'Designer'),
(47, 6, '_title-2', 'field_5fa02b7c3c841'),
(48, 6, 'subtitle', 'Arthur Besnehard'),
(49, 6, '_subtitle', 'field_5fa0292d05067'),
(50, 6, 'title-cv', 'Découvrez mon CV'),
(51, 6, '_title-cv', 'field_5fa0297b05068'),
(52, 6, 'link-cv', 'https://www.youtube.com/'),
(53, 6, '_link-cv', 'field_5fa0298c05069'),
(54, 6, 'icon', 'icon-youtube'),
(55, 6, '_icon', 'field_5fa0299b0506a'),
(56, 6, 'link-reseauxsociaux-1', 'https://www.youtube.com/'),
(57, 6, '_link-reseauxsociaux-1', 'field_5fa02a2d0506b'),
(58, 58, 'subtitle-1', 'Bonjour, je suis '),
(59, 58, '_subtitle-1', 'field_5fa02b3b3c83d'),
(60, 58, 'title-1', 'Arthur Besnehard'),
(61, 58, '_title-1', 'field_5fa02b4a3c83e'),
(62, 58, 'subtitle-2', 'Hello, I\'m'),
(63, 58, '_subtitle-2', 'field_5fa02b573c83f'),
(64, 58, 'title-2', 'Designer'),
(65, 58, '_title-2', 'field_5fa02b7c3c841'),
(66, 58, 'subtitle', 'Arthur Besnehard'),
(67, 58, '_subtitle', 'field_5fa0292d05067'),
(68, 58, 'title-cv', 'Découvrez mon CV'),
(69, 58, '_title-cv', 'field_5fa0297b05068'),
(70, 58, 'link-cv', 'https://www.youtube.com/'),
(71, 58, '_link-cv', 'field_5fa0298c05069'),
(72, 58, 'icon', 'icon-youtube'),
(73, 58, '_icon', 'field_5fa0299b0506a'),
(74, 58, 'link-reseauxsociaux-1', 'https://www.youtube.com/'),
(75, 58, '_link-reseauxsociaux-1', 'field_5fa02a2d0506b'),
(76, 60, '_edit_lock', '1605983167:1'),
(77, 31, '_edit_last', '1'),
(78, 31, 'adresse', 'zezezezezez'),
(79, 31, '_adresse', 'field_5f99750350bfb'),
(80, 31, 'telephone', '0606060606'),
(81, 31, '_telephone', 'field_5f99752850bfc'),
(82, 31, 'email', 'paul@mail.com'),
(83, 31, '_email', 'field_5f99754b50bfd'),
(84, 31, 'instagram', '@paulcollas'),
(85, 31, '_instagram', 'field_5f99755c50bfe'),
(86, 64, 'adresse', 'zezezezezez'),
(87, 64, '_adresse', 'field_5f99750350bfb'),
(88, 64, 'telephone', '0606060606'),
(89, 64, '_telephone', 'field_5f99752850bfc'),
(90, 64, 'email', 'paul@mail.com'),
(91, 64, '_email', 'field_5f99754b50bfd'),
(92, 64, 'instagram', '@paulcollas'),
(93, 64, '_instagram', 'field_5f99755c50bfe'),
(94, 66, 'subtitle-1', 'Bonjour, je suis '),
(95, 66, '_subtitle-1', 'field_5fa02b3b3c83d'),
(96, 66, 'title-1', 'Arthur Besnehard'),
(97, 66, '_title-1', 'field_5fa02b4a3c83e'),
(98, 66, 'subtitle-2', 'Hello, I\'m'),
(99, 66, '_subtitle-2', 'field_5fa02b573c83f'),
(100, 66, 'title-2', 'Designer'),
(101, 66, '_title-2', 'field_5fa02b7c3c841'),
(102, 66, 'subtitle', 'Arthur Besnehard'),
(103, 66, '_subtitle', 'field_5fa0292d05067'),
(104, 66, 'title-cv', 'Découvrez mon CV'),
(105, 66, '_title-cv', 'field_5fa0297b05068'),
(106, 66, 'link-cv', 'https://www.youtube.com/'),
(107, 66, '_link-cv', 'field_5fa0298c05069'),
(108, 66, 'icon', 'icon-youtube'),
(109, 66, '_icon', 'field_5fa0299b0506a'),
(110, 66, 'link-reseauxsociaux-1', 'https://www.youtube.com/'),
(111, 66, '_link-reseauxsociaux-1', 'field_5fa02a2d0506b'),
(113, 60, '_wp_page_template', 'about.php');

-- --------------------------------------------------------

--
-- Structure de la table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-09-29 19:42:09', '2020-09-29 17:42:09', '<!-- wp:paragraph -->\n<p>Bienvenue sur WordPress. Ceci est votre premier article. Modifiez-le ou supprimez-le, puis commencez à écrire !</p>\n<!-- /wp:paragraph -->', 'Bonjour tout le monde !', '', 'publish', 'open', 'open', '', 'bonjour-tout-le-monde', '', '', '2020-09-29 19:42:09', '2020-09-29 17:42:09', '', 0, 'http://localhost/07_MUCH/arthur/?p=1', 0, 'post', '', 1),
(2, 1, '2020-09-29 19:42:09', '2020-09-29 17:42:09', '<!-- wp:paragraph -->\n<p>Ceci est une page d’exemple. C’est différent d’un article de blog parce qu’elle restera au même endroit et apparaîtra dans la navigation de votre site (dans la plupart des thèmes). La plupart des gens commencent par une page « À propos » qui les présente aux visiteurs du site. Cela pourrait ressembler à quelque chose comme cela :</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Bonjour ! Je suis un mécanicien qui aspire à devenir acteur, et voici mon site. J’habite à Bordeaux, j’ai un super chien baptisé Russell, et j’aime la vodka-ananas (ainsi qu’être surpris par la pluie soudaine lors de longues balades sur la plage au coucher du soleil).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou quelque chose comme cela :</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>La société 123 Machin Truc a été créée en 1971, et n’a cessé de proposer au public des machins-trucs de qualité depuis lors. Située à Saint-Remy-en-Bouzemont-Saint-Genest-et-Isson, 123 Machin Truc emploie 2 000 personnes, et fabrique toutes sortes de bidules super pour la communauté bouzemontoise.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>En tant que nouvel utilisateur ou utilisatrice de WordPress, vous devriez vous rendre sur <a href=\"http://localhost/07_MUCH/arthur/wp-admin/\">votre tableau de bord</a> pour supprimer cette page et créer de nouvelles pages pour votre contenu. Amusez-vous bien !</p>\n<!-- /wp:paragraph -->', 'Page d’exemple', '', 'publish', 'closed', 'open', '', 'page-d-exemple', '', '', '2020-09-29 19:42:09', '2020-09-29 17:42:09', '', 0, 'http://localhost/07_MUCH/arthur/?page_id=2', 0, 'page', '', 0),
(3, 1, '2020-09-29 19:42:09', '2020-09-29 17:42:09', '<!-- wp:heading --><h2>Qui sommes-nous ?</h2><!-- /wp:heading --><!-- wp:paragraph --><p>L’adresse de notre site Web est : http://localhost/07_MUCH/arthur.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Utilisation des données personnelles collectées</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Commentaires</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Quand vous laissez un commentaire sur notre site web, les données inscrites dans le formulaire de commentaire, mais aussi votre adresse IP et l’agent utilisateur de votre navigateur sont collectés pour nous aider à la détection des commentaires indésirables.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Une chaîne anonymisée créée à partir de votre adresse de messagerie (également appelée hash) peut être envoyée au service Gravatar pour vérifier si vous utilisez ce dernier. Les clauses de confidentialité du service Gravatar sont disponibles ici : https://automattic.com/privacy/. Après validation de votre commentaire, votre photo de profil sera visible publiquement à coté de votre commentaire.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Médias</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Si vous êtes un utilisateur ou une utilisatrice enregistré·e et que vous téléversez des images sur le site web, nous vous conseillons d’éviter de téléverser des images contenant des données EXIF de coordonnées GPS. Les visiteurs de votre site web peuvent télécharger et extraire des données de localisation depuis ces images.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formulaires de contact</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Si vous déposez un commentaire sur notre site, il vous sera proposé d’enregistrer votre nom, adresse de messagerie et site web dans des cookies. C’est uniquement pour votre confort afin de ne pas avoir à saisir ces informations si vous déposez un autre commentaire plus tard. Ces cookies expirent au bout d’un an.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Si vous vous rendez sur la page de connexion, un cookie temporaire sera créé afin de déterminer si votre navigateur accepte les cookies. Il ne contient pas de données personnelles et sera supprimé automatiquement à la fermeture de votre navigateur.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Lorsque vous vous connecterez, nous mettrons en place un certain nombre de cookies pour enregistrer vos informations de connexion et vos préférences d’écran. La durée de vie d’un cookie de connexion est de deux jours, celle d’un cookie d’option d’écran est d’un an. Si vous cochez « Se souvenir de moi », votre cookie de connexion sera conservé pendant deux semaines. Si vous vous déconnectez de votre compte, le cookie de connexion sera effacé.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>En modifiant ou en publiant une publication, un cookie supplémentaire sera enregistré dans votre navigateur. Ce cookie ne comprend aucune donnée personnelle. Il indique simplement l’ID de la publication que vous venez de modifier. Il expire au bout d’un jour.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contenu embarqué depuis d’autres sites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Les articles de ce site peuvent inclure des contenus intégrés (par exemple des vidéos, images, articles…). Le contenu intégré depuis d’autres sites se comporte de la même manière que si le visiteur se rendait sur cet autre site.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Ces sites web pourraient collecter des données sur vous, utiliser des cookies, embarquer des outils de suivis tiers, suivre vos interactions avec ces contenus embarqués si vous disposez d’un compte connecté sur leur site web.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Statistiques et mesures d’audience</h3><!-- /wp:heading --><!-- wp:heading --><h2>Utilisation et transmission de vos données personnelles</h2><!-- /wp:heading --><!-- wp:heading --><h2>Durées de stockage de vos données</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Si vous laissez un commentaire, le commentaire et ses métadonnées sont conservés indéfiniment. Cela permet de reconnaître et approuver automatiquement les commentaires suivants au lieu de les laisser dans la file de modération.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Pour les utilisateurs et utilisatrices qui s’inscrivent sur notre site (si cela est possible), nous stockons également les données personnelles indiquées dans leur profil. Tous les utilisateurs et utilisatrices peuvent voir, modifier ou supprimer leurs informations personnelles à tout moment (à l’exception de leur nom d’utilisateur·ice). Les gestionnaires du site peuvent aussi voir et modifier ces informations.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Les droits que vous avez sur vos données</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Si vous avez un compte ou si vous avez laissé des commentaires sur le site, vous pouvez demander à recevoir un fichier contenant toutes les données personnelles que nous possédons à votre sujet, incluant celles que vous nous avez fournies. Vous pouvez également demander la suppression des données personnelles vous concernant. Cela ne prend pas en compte les données stockées à des fins administratives, légales ou pour des raisons de sécurité.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Transmission de vos données personnelles</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Les commentaires des visiteurs peuvent être vérifiés à l’aide d’un service automatisé de détection des commentaires indésirables.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Informations de contact</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informations supplémentaires</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comment nous protégeons vos données</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Procédures mises en œuvre en cas de fuite de données</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Les services tiers qui nous transmettent des données</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Opérations de marketing automatisé et/ou de profilage réalisées à l’aide des données personnelles</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Affichage des informations liées aux secteurs soumis à des régulations spécifiques</h3><!-- /wp:heading -->', 'Politique de confidentialité', '', 'draft', 'closed', 'open', '', 'politique-de-confidentialite', '', '', '2020-09-29 19:42:09', '2020-09-29 17:42:09', '', 0, 'http://localhost/07_MUCH/arthur/?page_id=3', 0, 'page', '', 0),
(6, 1, '2020-10-22 23:22:27', '2020-10-22 21:22:27', 'Je m’appelle Arthur Besnehard et je suis vidéaste freelance sous le nom de InMedia Production. Après avoir été diplômé d’un DUT information et communication à l’Université de Caen, je poursuis mes étude en école de commerce en Bachelor Communication à l’ESG Rennes. C’est d’ailleurs dans cet établissement que je suis en alternance en tant que chargé de communication.\nÀ travers mon parcours scolaire et personnel j’ai pu rencontrer et travailler avec des entreprises et des prestataires dans divers domaines qui m’ont amenés à produire un certain nombre de réalisations audiovisuelles professionnelles.\nDepuis tout jeune je suis passionné par le monde de la vidéo et de la création en général. Cela a commencé avec les célèbres montages de vacances pour aujourd’hui produire du contenu professionnel auprès d’entreprises et différents prospects.\nJe me spécialise dans la réalisations de vidéo corporate, artistique et du reportage.', 'accueil', '', 'publish', 'closed', 'closed', '', 'accueil', '', '', '2020-11-16 23:27:58', '2020-11-16 22:27:58', '', 0, 'http://localhost/07_MUCH/arthur/?page_id=6', 0, 'page', '', 0),
(7, 1, '2020-10-22 23:22:27', '2020-10-22 21:22:27', '', 'accueil', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2020-10-22 23:22:27', '2020-10-22 21:22:27', '', 6, 'http://localhost/07_MUCH/arthur/2020/10/22/6-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2020-10-27 14:32:55', '2020-10-27 13:32:55', 'vhshihvsr\r\nqvjnktr\r\n<img src=\"http://localhost/07_MUCH/arthur/wp-content/uploads/2020/10/0-300x300.jpg\" alt=\"\" width=\"300\" height=\"300\" class=\"alignnone size-medium wp-image-23\" />\r\nfcvsdfvsrf', 'test', 'test', 'publish', 'closed', 'closed', '', 'test', '', '', '2020-10-28 11:49:26', '2020-10-28 10:49:26', '', 0, 'http://localhost/07_MUCH/arthur/?post_type=realisations&#038;p=10', 0, 'realisations', '', 0),
(11, 1, '2020-10-27 14:32:55', '2020-10-27 13:32:55', 'test', 'test', 'test', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2020-10-27 14:32:55', '2020-10-27 13:32:55', '', 10, 'http://localhost/07_MUCH/arthur/10-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2020-10-28 11:38:58', '2020-10-28 10:38:58', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:12:\"realisations\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Réalisation', 'realisation', 'publish', 'closed', 'closed', '', 'group_5f9949f783a7a', '', '', '2020-10-28 11:48:29', '2020-10-28 10:48:29', '', 0, 'http://localhost/07_MUCH/arthur/?post_type=acf-field-group&#038;p=17', 0, 'acf-field-group', '', 0),
(21, 1, '2020-10-28 11:45:25', '2020-10-28 10:45:25', '', 'test', 'test', 'inherit', 'closed', 'closed', '', '10-autosave-v1', '', '', '2020-10-28 11:45:25', '2020-10-28 10:45:25', '', 10, 'http://localhost/07_MUCH/arthur/10-autosave-v1/', 0, 'revision', '', 0),
(22, 1, '2020-10-28 11:48:29', '2020-10-28 10:48:29', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'youtube', 'youtube', 'publish', 'closed', 'closed', '', 'field_5f994c72222b3', '', '', '2020-10-28 11:48:29', '2020-10-28 10:48:29', '', 17, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&p=22', 0, 'acf-field', '', 0),
(23, 1, '2020-10-28 11:49:09', '2020-10-28 10:49:09', '', '0', '', 'inherit', 'open', 'closed', '', '0', '', '', '2020-10-28 11:49:09', '2020-10-28 10:49:09', '', 10, 'http://localhost/07_MUCH/arthur/wp-content/uploads/2020/10/0.jpg', 0, 'attachment', 'image/jpeg', 0),
(24, 1, '2020-10-28 11:49:26', '2020-10-28 10:49:26', 'vhshihvsr\r\nqvjnktr\r\n<img src=\"http://localhost/07_MUCH/arthur/wp-content/uploads/2020/10/0-300x300.jpg\" alt=\"\" width=\"300\" height=\"300\" class=\"alignnone size-medium wp-image-23\" />\r\nfcvsdfvsrf', 'test', 'test', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2020-10-28 11:49:26', '2020-10-28 10:49:26', '', 10, 'http://localhost/07_MUCH/arthur/10-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2020-10-28 11:53:36', '2020-10-28 10:53:36', '<!-- wp:paragraph -->\n<p>Ceci est une page d’exemple. C’est différent d’un article de blog parce qu’elle restera au même endroit et apparaîtra dans la navigation de votre site (dans la plupart des thèmes). La plupart des gens commencent par une page «&nbsp;À propos&nbsp;» qui les présente aux visiteurs du site. Cela pourrait ressembler à quelque chose comme cela&nbsp;:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Bonjour&nbsp;! Je suis un mécanicien qui aspire à devenir acteur, et voici mon site. J’habite à Bordeaux, j’ai un super chien baptisé Russell, et j’aime la vodka-ananas (ainsi qu’être surpris par la pluie soudaine lors de longues balades sur la plage au coucher du soleil).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou quelque chose comme cela&nbsp;:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>La société 123 Machin Truc a été créée en 1971, et n’a cessé de proposer au public des machins-trucs de qualité depuis lors. Située à Saint-Remy-en-Bouzemont-Saint-Genest-et-Isson, 123 Machin Truc emploie 2 000 personnes, et fabrique toutes sortes de bidules super pour la communauté bouzemontoise.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>En tant que nouvel utilisateur ou utilisatrice de WordPress, vous devriez vous rendre sur <a href=\"http://localhost/07_MUCH/arthur/wp-admin/\">votre tableau de bord</a> pour supprimer cette page et créer de nouvelles pages pour votre contenu. Amusez-vous bien&nbsp;!</p>\n<!-- /wp:paragraph -->', 'Page d’exemple', '', 'inherit', 'closed', 'closed', '', '2-autosave-v1', '', '', '2020-10-28 11:53:36', '2020-10-28 10:53:36', '', 2, 'http://localhost/07_MUCH/arthur/2-autosave-v1/', 0, 'revision', '', 0),
(28, 1, '2020-10-28 14:24:43', '2020-10-28 13:24:43', '<!-- wp:booking/booking {\"wpbc_shortcode\":\"[booking]\"} -->\n<div class=\"wp-block-booking-booking\">[booking]</div>\n<!-- /wp:booking/booking -->', 'A propos', '', 'publish', 'closed', 'closed', '', 'a-propos', '', '', '2020-12-23 10:24:29', '2020-12-23 09:24:29', '', 0, 'http://localhost/07_MUCH/arthur/?page_id=28', 0, 'page', '', 0),
(29, 1, '2020-10-28 14:24:43', '2020-10-28 13:24:43', '', 'A propos', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2020-10-28 14:24:43', '2020-10-28 13:24:43', '', 28, 'http://localhost/07_MUCH/arthur/28-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2020-10-28 14:29:26', '2020-10-28 13:29:26', '', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2020-11-07 17:59:39', '2020-11-07 16:59:39', '', 0, 'http://localhost/07_MUCH/arthur/?page_id=31', 0, 'page', '', 0),
(32, 1, '2020-10-28 14:29:26', '2020-10-28 13:29:26', '', 'Contact', '', 'inherit', 'closed', 'closed', '', '31-revision-v1', '', '', '2020-10-28 14:29:26', '2020-10-28 13:29:26', '', 31, 'http://localhost/07_MUCH/arthur/31-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2020-10-28 14:43:18', '2020-10-28 13:43:18', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"31\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Contact', 'contact', 'publish', 'closed', 'closed', '', 'group_5f9974ff24f75', '', '', '2020-10-28 14:43:18', '2020-10-28 13:43:18', '', 0, 'http://localhost/07_MUCH/arthur/?post_type=acf-field-group&#038;p=36', 0, 'acf-field-group', '', 0),
(37, 1, '2020-10-28 14:43:18', '2020-10-28 13:43:18', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Adresse', 'adresse', 'publish', 'closed', 'closed', '', 'field_5f99750350bfb', '', '', '2020-10-28 14:43:18', '2020-10-28 13:43:18', '', 36, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&p=37', 0, 'acf-field', '', 0),
(38, 1, '2020-10-28 14:43:18', '2020-10-28 13:43:18', 'a:12:{s:4:\"type\";s:6:\"number\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:4:\"step\";s:0:\"\";}', 'Téléphone', 'telephone', 'publish', 'closed', 'closed', '', 'field_5f99752850bfc', '', '', '2020-10-28 14:43:18', '2020-10-28 13:43:18', '', 36, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&p=38', 1, 'acf-field', '', 0),
(39, 1, '2020-10-28 14:43:18', '2020-10-28 13:43:18', 'a:9:{s:4:\"type\";s:5:\"email\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";}', 'Email', 'email', 'publish', 'closed', 'closed', '', 'field_5f99754b50bfd', '', '', '2020-10-28 14:43:18', '2020-10-28 13:43:18', '', 36, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&p=39', 2, 'acf-field', '', 0),
(40, 1, '2020-10-28 14:43:18', '2020-10-28 13:43:18', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Instagram', 'instagram', 'publish', 'closed', 'closed', '', 'field_5f99755c50bfe', '', '', '2020-10-28 14:43:18', '2020-10-28 13:43:18', '', 36, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&p=40', 3, 'acf-field', '', 0),
(46, 1, '2020-11-02 16:48:27', '2020-11-02 15:48:27', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"6\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'About', 'about', 'publish', 'closed', 'closed', '', 'group_5fa0292725b30', '', '', '2020-11-02 16:48:46', '2020-11-02 15:48:46', '', 0, 'http://localhost/07_MUCH/arthur/?post_type=acf-field-group&#038;p=46', 0, 'acf-field-group', '', 0),
(47, 1, '2020-11-02 16:48:27', '2020-11-02 15:48:27', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:16:\"Arthur Besnehard\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'subtitle', 'subtitle', 'publish', 'closed', 'closed', '', 'field_5fa0292d05067', '', '', '2020-11-02 16:48:27', '2020-11-02 15:48:27', '', 46, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&p=47', 0, 'acf-field', '', 0),
(48, 1, '2020-11-02 16:48:27', '2020-11-02 15:48:27', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'title-cv', 'title-cv', 'publish', 'closed', 'closed', '', 'field_5fa0297b05068', '', '', '2020-11-02 16:48:27', '2020-11-02 15:48:27', '', 46, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&p=48', 1, 'acf-field', '', 0),
(49, 1, '2020-11-02 16:48:27', '2020-11-02 15:48:27', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'link-cv', 'link-cv', 'publish', 'closed', 'closed', '', 'field_5fa0298c05069', '', '', '2020-11-02 16:48:27', '2020-11-02 15:48:27', '', 46, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&p=49', 2, 'acf-field', '', 0),
(50, 1, '2020-11-02 16:48:27', '2020-11-02 15:48:27', 'a:13:{s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:5:{s:13:\"icon-linkedin\";s:8:\"Linkedin\";s:13:\"icon-facebook\";s:8:\"Facebook\";s:14:\"icon-instagram\";s:9:\"Instagram\";s:12:\"icon-twitter\";s:7:\"Twitter\";s:12:\"icon-youtube\";s:7:\"Youtube\";}s:13:\"default_value\";b:0;s:10:\"allow_null\";i:1;s:8:\"multiple\";i:0;s:2:\"ui\";i:0;s:13:\"return_format\";s:5:\"value\";s:4:\"ajax\";i:0;s:11:\"placeholder\";s:0:\"\";}', 'icon', 'icon', 'publish', 'closed', 'closed', '', 'field_5fa0299b0506a', '', '', '2020-11-02 16:48:27', '2020-11-02 15:48:27', '', 46, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&p=50', 3, 'acf-field', '', 0),
(51, 1, '2020-11-02 16:48:27', '2020-11-02 15:48:27', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'link-réseauxsociaux-1', 'link-reseauxsociaux-1', 'publish', 'closed', 'closed', '', 'field_5fa02a2d0506b', '', '', '2020-11-02 16:48:27', '2020-11-02 15:48:27', '', 46, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&p=51', 4, 'acf-field', '', 0),
(53, 1, '2020-11-02 16:53:45', '2020-11-02 15:53:45', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"6\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Slider-accueil', 'slider-accueil', 'publish', 'closed', 'closed', '', 'group_5fa02a8f79e4e', '', '', '2020-11-16 23:34:12', '2020-11-16 22:34:12', '', 0, 'http://localhost/07_MUCH/arthur/?post_type=acf-field-group&#038;p=53', 0, 'acf-field-group', '', 0),
(54, 1, '2020-11-02 16:53:45', '2020-11-02 15:53:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'subtitle-1', 'subtitle-1', 'publish', 'closed', 'closed', '', 'field_5fa02b3b3c83d', '', '', '2020-11-16 23:34:12', '2020-11-16 22:34:12', '', 53, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&#038;p=54', 0, 'acf-field', '', 0),
(55, 1, '2020-11-02 16:53:45', '2020-11-02 15:53:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'title-1', 'title-1', 'publish', 'closed', 'closed', '', 'field_5fa02b4a3c83e', '', '', '2020-11-02 16:53:45', '2020-11-02 15:53:45', '', 53, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&p=55', 1, 'acf-field', '', 0),
(56, 1, '2020-11-02 16:53:45', '2020-11-02 15:53:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'subtitle-2', 'subtitle-2', 'publish', 'closed', 'closed', '', 'field_5fa02b573c83f', '', '', '2020-11-02 16:53:45', '2020-11-02 15:53:45', '', 53, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&p=56', 2, 'acf-field', '', 0),
(57, 1, '2020-11-02 16:53:45', '2020-11-02 15:53:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'title-2', 'title-2', 'publish', 'closed', 'closed', '', 'field_5fa02b7c3c841', '', '', '2020-11-02 16:53:45', '2020-11-02 15:53:45', '', 53, 'http://localhost/07_MUCH/arthur/?post_type=acf-field&p=57', 3, 'acf-field', '', 0),
(58, 1, '2020-11-02 16:55:26', '2020-11-02 15:55:26', '', 'accueil', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2020-11-02 16:55:26', '2020-11-02 15:55:26', '', 6, 'http://localhost/07_MUCH/arthur/6-revision-v1/', 0, 'revision', '', 0),
(60, 1, '2020-11-02 20:24:33', '2020-11-02 19:24:33', '', 'about', '', 'publish', 'closed', 'closed', '', 'skills', '', '', '2020-11-21 19:27:58', '2020-11-21 18:27:58', '', 0, 'http://localhost/07_MUCH/arthur/?page_id=60', 0, 'page', '', 0),
(61, 1, '2020-11-02 20:24:33', '2020-11-02 19:24:33', '', 'skills', '', 'inherit', 'closed', 'closed', '', '60-revision-v1', '', '', '2020-11-02 20:24:33', '2020-11-02 19:24:33', '', 60, 'http://localhost/07_MUCH/arthur/60-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2020-11-07 17:59:39', '2020-11-07 16:59:39', '', 'Contact', '', 'inherit', 'closed', 'closed', '', '31-revision-v1', '', '', '2020-11-07 17:59:39', '2020-11-07 16:59:39', '', 31, 'http://localhost/07_MUCH/arthur/2020/11/31-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2020-11-07 17:59:43', '2020-11-07 16:59:43', '', 'Contact', '', 'inherit', 'closed', 'closed', '', '31-autosave-v1', '', '', '2020-11-07 17:59:43', '2020-11-07 16:59:43', '', 31, 'http://localhost/07_MUCH/arthur/2020/11/31-autosave-v1/', 0, 'revision', '', 0),
(66, 1, '2020-11-16 23:25:19', '2020-11-16 22:25:19', 'Je m’appelle Arthur Besnehard et je suis vidéaste freelance sous le nom de InMedia Production. Après avoir été diplômé d’un DUT information et communication à l’Université de Caen, je poursuis mes étude en école de commerce en Bachelor Communication à l’ESG Rennes. C’est d’ailleurs dans cet établissement que je suis en alternance en tant que chargé de communication.\nÀ travers mon parcours scolaire et personnel j’ai pu rencontrer et travailler avec des entreprises et des prestataires dans divers domaines qui m’ont amenés à produire un certain nombre de réalisations audiovisuelles professionnelles.\nDepuis tout jeune je suis passionné par le monde de la vidéo et de la création en général. Cela a commencé avec les célèbres montages de vacances pour aujourd’hui produire du contenu professionnel auprès d’entreprises et différents prospects.\nJe me spécialise dans la réalisations de vidéo corporate, artistique et du reportage.', 'accueil', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2020-11-16 23:25:19', '2020-11-16 22:25:19', '', 6, 'http://localhost/07_MUCH/arthur/2020/11/6-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2020-11-21 19:17:55', '2020-11-21 18:17:55', '', 'skills', '', 'inherit', 'closed', 'closed', '', '60-autosave-v1', '', '', '2020-11-21 19:17:55', '2020-11-21 18:17:55', '', 60, 'http://localhost/07_MUCH/arthur/2020/11/60-autosave-v1/', 0, 'revision', '', 0),
(70, 1, '2020-11-21 19:27:58', '2020-11-21 18:27:58', '', 'about', '', 'inherit', 'closed', 'closed', '', '60-revision-v1', '', '', '2020-11-21 19:27:58', '2020-11-21 18:27:58', '', 60, 'http://localhost/07_MUCH/arthur/2020/11/60-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2020-12-23 10:24:29', '2020-12-23 09:24:29', '<!-- wp:booking/booking {\"wpbc_shortcode\":\"[booking]\"} -->\n<div class=\"wp-block-booking-booking\">[booking]</div>\n<!-- /wp:booking/booking -->', 'A propos', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2020-12-23 10:24:29', '2020-12-23 09:24:29', '', 28, 'http://localhost/07_MUCH/arthur/2020/12/28-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2020-12-23 10:40:55', '2020-12-23 09:40:55', '<!-- wp:booking/booking {\"wpbc_shortcode\":\"[booking]\"} -->\n<div class=\"wp-block-booking-booking\">[booking]</div>\n<!-- /wp:booking/booking -->', 'A propos', '', 'inherit', 'closed', 'closed', '', '28-autosave-v1', '', '', '2020-12-23 10:40:55', '2020-12-23 09:40:55', '', 28, 'http://localhost/07_MUCH/arthur/2020/12/28-autosave-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Non classé', 'non-classe', 0),
(2, 'formation', 'formation', 0),
(3, 'experiences', 'experience', 0),
(4, 'competence', 'competence', 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'category', '', 0, 0),
(3, 3, 'category', '', 0, 0),
(4, 4, 'category', '', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'arthur'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"8aaba077a3703d7a6f472183ec13d7d81ebe0026e19d08497b1407841c85550f\";a:4:{s:10:\"expiration\";i:1614683042;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36\";s:5:\"login\";i:1614510242;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '68'),
(18, 1, 'closedpostboxes_realisations', 'a:0:{}'),
(19, 1, 'metaboxhidden_realisations', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(20, 1, 'meta-box-order_realisations', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:22:\"submitdiv,postimagediv\";s:6:\"normal\";s:53:\"postexcerpt,acf-group_5f9949f783a7a,slugdiv,authordiv\";s:8:\"advanced\";s:0:\"\";}'),
(21, 1, 'screen_layout_realisations', '2'),
(22, 1, 'wp_user-settings', 'post_dfw=off&libraryContent=browse'),
(23, 1, 'wp_user-settings-time', '1603882161'),
(24, 1, 'closedpostboxes_page', 'a:0:{}'),
(25, 1, 'metaboxhidden_page', 'a:0:{}'),
(26, 1, 'meta-box-order_page', 'a:4:{s:6:\"normal\";s:47:\"acf-group_5fa02a8f79e4e,acf-group_5fa0292725b30\";s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:0:\"\";s:8:\"advanced\";s:0:\"\";}');

-- --------------------------------------------------------

--
-- Structure de la table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'arthur', '$P$B.4IJTbYAmajcwmu/8.zwJgowk0XKn.', 'arthur', 'paulcollas446@gmail.com', 'http://localhost/07_MUCH/arthur', '2020-09-29 17:42:09', '', 0, 'arthur');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `wp_booking`
--
ALTER TABLE `wp_booking`
  ADD PRIMARY KEY (`booking_id`);

--
-- Index pour la table `wp_bookingdates`
--
ALTER TABLE `wp_bookingdates`
  ADD PRIMARY KEY (`booking_dates_id`),
  ADD UNIQUE KEY `booking_id_dates` (`booking_id`,`booking_date`);

--
-- Index pour la table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Index pour la table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Index pour la table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Index pour la table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Index pour la table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Index pour la table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Index pour la table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Index pour la table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `wp_booking`
--
ALTER TABLE `wp_booking`
  MODIFY `booking_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `wp_bookingdates`
--
ALTER TABLE `wp_bookingdates`
  MODIFY `booking_dates_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=643;

--
-- AUTO_INCREMENT pour la table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT pour la table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT pour la table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT pour la table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
